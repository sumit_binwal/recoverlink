//
//  AppDelegate.h
//  RecoverLinks
//
//  Created by  on 17/02/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#define AppDel (AppDelegate *)[[UIApplication sharedApplication] delegate];
@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>
{
    UILocalNotification *localnotification;
}

@property (strong,nonatomic)IBOutlet UIButton *btnHome;
@property(strong,nonatomic)IBOutlet  UIButton *btnLearn;
@property(strong,nonatomic)IBOutlet UIButton *btnContact;
@property (weak, nonatomic) IBOutlet UIView *cstmTabBar;
@property(weak,nonatomic)IBOutlet UIButton *btnSetting;
@property(strong,nonatomic)UILocalNotification *localnotification;
@property (weak, nonatomic) IBOutlet UINavigationController *navigationHelp;
@property (nonatomic,strong)    UILocalNotification *notification;
@property (strong, nonatomic)IBOutlet UIWindow *window;
@property (strong, nonatomic)IBOutlet UINavigationController *navigationControl;
@property(strong,nonatomic)IBOutlet UITabBarController *tabBar;
@property (weak, nonatomic) IBOutlet UINavigationController *navControl;
- (void)registerForRemoteNotification ;
-(void)setUpTabBar;
-(void)allocTabBarTipControllar:(UIViewController *)tvc;
-(void)getCurrentDay;
-(IBAction)customeTabBarButtonClicked:(UIButton *)sender;
-(void)allocTabBarControllar;
-(void)allocTabBarAfter30Days;

- (void)registerForRemoteNotificationNextDay;
@end

