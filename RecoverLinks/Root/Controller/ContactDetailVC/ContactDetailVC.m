//
//  ContactDetailVC.m
//  RecoverLinks
//
//  Created by  on 12/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "ContactDetailVC.h"

@interface ContactDetailVC ()
{
    IBOutlet UILabel *lblPhnWrk;
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblAddress;
    IBOutlet UILabel *lblEmail;
    IBOutlet UILabel *lblPhnNumber;
    IBOutlet UILabel *lblPost;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIButton *btnPhnWorkCall;
    IBOutlet UILabel *lblPhoneWork;
}
@end

@implementation ContactDetailVC
@synthesize dataDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"CONTACTS"];
    lblName.text=[[NSString stringWithFormat:@"%@ %@",[dataDict objectForKey:@"FirstName"],[dataDict objectForKey:@"LastName"]] uppercaseString];
    lblPost.text=[[[dataDict objectForKey:@"Title"] lowercaseString]capitalizedString];
 
    if([self isNotNull:[dataDict objectForKey:@"PhoneWork"]])
    {
    lblPhnWrk.text=[dataDict objectForKey:@"PhoneWork"];
    }
    else
    {
        lblPhnWrk.text=@"";
        [btnPhnWorkCall setHidden:YES];
        [lblPhoneWork setHidden:YES];
    }

    if([self isNotNull:[dataDict objectForKey:@"Email"]])
    {
        lblEmail.text=[dataDict objectForKey:@"Email"];
    }
    else
    {
        lblEmail.text=@"";

    }
    
    lblPhnNumber.text=[dataDict objectForKey:@"Phone"];

    if([self isNotNull:[dataDict objectForKey:@"Address1"]])
    {
        
    lblAddress.text=[[[NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@",[dataDict objectForKey:@"Address1"],[dataDict objectForKey:@"Address2"],[dataDict objectForKey:@"City"],[dataDict objectForKey:@"Country"],[dataDict objectForKey:@"StateName"],[dataDict objectForKey:@"Zipcode"]] lowercaseString] capitalizedString];
    }
    else
    {
    lblAddress.text=@"";
    }
        [scrllVw setScrollEnabled:YES];
}

-(void)viewDidLayoutSubviews
{
    if([UIScreen mainScreen].bounds.size.height<568)
    {

        [scrllVw setContentSize:CGSizeMake(320.0f, 500.0f+lblAddress.frame.size.height)];
    }
    else
    {
        int i=lblAddress.frame.size.height+lblAddress.frame.origin.y+100;
        if (lblAddress.frame.size.height+lblAddress.frame.origin.y+60>568) {
            [scrllVw setScrollEnabled:YES];
               [scrllVw setContentSize:CGSizeMake(320.0f, lblAddress.frame.origin.y+lblAddress.frame.size.height)];
        }
        else
        {
            [scrllVw setScrollEnabled:NO];
        }

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)callButtonClicked:(id)sender {
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {

        NSString *phone=[NSString stringWithFormat:@"%@",[dataDict objectForKey:@"Phone"]];

        phone=[CommonFunctions trimSpaceInString:phone];
        
        NSString *phoneNumber = [@"tel://" stringByAppendingString:phone];
        

        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
    else
    {
        if([self isNotNull:[dataDict objectForKey:@"PhoneWork"]])
        {
            NSString *phoneNumber = [@"tel://" stringByAppendingString:[CommonFunctions trimSpaceInString:[dataDict objectForKey:@"PhoneWork"]]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Phone Number Not Found."];
        }
    }
}
@end
