//
//  FaceTrackerVC.m
//  RecoverLinks
//
//  Created by  on 02/04/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "FaceTrackerVC.h"
#import "MoodTrackerCell.h"
@interface FaceTrackerVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    

    IBOutlet UICollectionView *faceCollectionView;
    NSMutableArray *faceDataArr;
}
@end

@implementation FaceTrackerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [faceCollectionView registerNib:[UINib nibWithNibName:@"MoodTrackerCell" bundle:nil] forCellWithReuseIdentifier:@"MoodTrackerCell"];
   
    

}
-(void)viewWillAppear:(BOOL)animated
{
    if ([CommonFunctions reachabiltyCheck]) {
        
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getFaceGraphData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your network connection."];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark-UICollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return faceDataArr.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MoodTrackerCell *cell=[faceCollectionView dequeueReusableCellWithReuseIdentifier:@"MoodTrackerCell" forIndexPath:indexPath];
    
    if([[[faceDataArr objectAtIndex:indexPath.row] objectForKey:@"Value"] isEqualToString:@"5"])
    {
                [cell.face1 setHidden:YES];
                [cell.face2 setHidden:YES];
                [cell.face3 setHidden:YES];
                [cell.face4 setHidden:YES];
        
        
        [cell.face5 setBackgroundColor:[UIColor colorWithRed:136.0f/255.0f green:185.0f/255.0f blue:54.0f/255.0f alpha:1]];
        [cell.face5 setHidden:NO];
    }
    else if([[[faceDataArr objectAtIndex:indexPath.row] objectForKey:@"Value"] isEqualToString:@"4"])
    {
        [cell.face1 setHidden:YES];
        [cell.face2 setHidden:YES];
        [cell.face3 setHidden:YES];
        [cell.face5 setHidden:YES];

        [cell.face4 setBackgroundColor:[UIColor colorWithRed:136.0f/255.0f green:185.0f/255.0f blue:54.0f/255.0f alpha:1]];
        [cell.face4 setHidden:NO];
    }
    else if([[[faceDataArr objectAtIndex:indexPath.row] objectForKey:@"Value"] isEqualToString:@"3"])
    {
        [cell.face1 setHidden:YES];
        [cell.face2 setHidden:YES];
        [cell.face4 setHidden:YES];
        [cell.face5 setHidden:YES];
        [cell.face3 setBackgroundColor:[UIColor colorWithRed:220.0f/255.0f green:202.0f/255.0f blue:47.0f/255.0f alpha:1]];
        [cell.face3 setImage:[UIImage imageNamed:@""]];
        [cell.face3 setHidden:NO];
    }
    else if([[[faceDataArr objectAtIndex:indexPath.row] objectForKey:@"Value"] isEqualToString:@"2"])
    {
        [cell.face1 setHidden:YES];
        [cell.face3 setHidden:YES];
        [cell.face4 setHidden:YES];
        [cell.face5 setHidden:YES];
        [cell.face2 setBackgroundColor:[UIColor colorWithRed:185.0f/255.0f green:-0.0f/255.0f blue:36.0f/255.0f alpha:1]];
        [cell.face2 setHidden:NO];
    }
    else if([[[faceDataArr objectAtIndex:indexPath.row] objectForKey:@"Value"] isEqualToString:@"1"])
    {
        [cell.face3 setHidden:YES];
        [cell.face2 setHidden:YES];
        [cell.face4 setHidden:YES];
        [cell.face5 setHidden:YES];
        [cell.face1 setBackgroundColor:[UIColor colorWithRed:185.0f/255.0f green:-0.0f/255.0f blue:36.0f/255.0f alpha:1]];
        [cell.face1 setHidden:NO];
        
    }
    else
    {
        [cell.face3 setImage:[UIImage imageNamed:@"cross"]];
        [cell.face3 setHidden:NO];
        [cell.face3 setBackgroundColor:[UIColor clearColor]];
        [cell.face1 setHidden:YES];
        [cell.face2 setHidden:YES];
        [cell.face4 setHidden:YES];
        [cell.face5 setHidden:YES];
    }
    return cell;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([UIScreen mainScreen].bounds.size.height==736)
    {
    return CGSizeMake(7.0f, 251.0f);
    }
    else
    {
    return CGSizeMake(7.0f, 251.0f);
    }
}

#pragma mark- Calling Web API's

-(void)getFaceGraphData
{
    //  NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:validToken,@"token",txtConfirmPassword.text,@"password", nil];
    NSString *url = [NSString stringWithFormat:@"QuestionsGraph?Id=%@&QuestionId=1",[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID]];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/QuestionsGraph?Id=1&QuestionId=1
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];


        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [self performSelectorOnMainThread:@selector(fetchQuestionNo1Data:) withObject:responseDict waitUntilDone:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}
-(void)fetchQuestionNo1Data:(NSDictionary *)dict
{
    faceDataArr=[[NSMutableArray alloc]init];

    for (int i=0; i<[[dict objectForKey:@"replyData"]count]; i++) {
        [faceDataArr addObject:[[dict objectForKey:@"replyData"]objectAtIndex:i]];
    }
    [faceCollectionView reloadData];
}
@end
