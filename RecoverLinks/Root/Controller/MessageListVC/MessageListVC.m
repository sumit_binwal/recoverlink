//
//  MessageListVC.m
//  RecoverLinks
//
//  Created by  on 13/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "MessageListVC.h"
#import "MessageCustomeCell.h"
#import "MessageVC.h"
@interface MessageListVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *messageListArr;
    IBOutlet UITableView *tblView;
    IBOutlet UILabel *lblMsgError;
    NSString *srvrDateString;
    UIRefreshControl *refreshControl;
}
@end

@implementation MessageListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonFunctions setNavigationBar:self.navigationController];

    [self.navigationItem setTitle:@"MESSAGES"];
    
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonClicked)];
    [self.navigationItem setLeftBarButtonItem:leftBtn];
    leftBtn.imageInsets = UIEdgeInsetsMake(2, -5, 0, 0);
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"vcName"]isEqualToString:@"fromQuestion"])
    {
        [self.navigationItem setTitle:@"QUESTIONS"];
        [tblView setHidden:YES];
        [lblMsgError setHidden:NO];
        [lblMsgError setText:@"This app is read-only. You will no longer be able to receive messages."];
    }
    else
    {
        
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getMessageList];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Check your Network Connection."];
        }
    }
    // Do any additional setup after loading the view from its nib.
    refreshControl = [[UIRefreshControl alloc]
                      init];
    refreshControl.tintColor = [UIColor blueColor];
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblView addSubview:refreshControl];

   }

-(void)pullToRefresh
{
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getMessageList];
        [self performSelector:@selector(updateTable) withObject:nil
                   afterDelay:1];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your Network Connection."];
    }
}
-(void)updateTable
{
    [refreshControl endRefreshing];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"vcName"]isEqualToString:@"fromQuestion"])
    {
        [self.navigationItem setTitle:@"QUESTIONS"];
        [tblView setHidden:YES];
        [lblMsgError setHidden:NO];
        [lblMsgError setText:@"This app is read-only. You will no longer be able to receive messages."];
    }
    else
    {
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getMessageList];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Check your Network Connection."];
        }
    }
    

}

-(void)leftBarButtonClicked
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"vcName"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark-UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return messageListArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cellIdentifier";
    MessageCustomeCell *cell=[[MessageCustomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if(cell==nil)
    {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;

    NSArray *paths = [tableView indexPathsForVisibleRows];
    
    //  For getting the cells themselves
    
//    if (messageListArr.count == paths.count) {
//        tableView.scrollEnabled=NO;
//    }
//    else
//    {
//        tableView.scrollEnabled=YES;
//    }

    
    
    if(indexPath.row==0)
    {
        [cell.imgDevider setHidden:NO];
    }
    
    else
    {
        [cell.imgDevider setHidden:YES];
    }
    if(![self isNotNull:[[messageListArr objectAtIndex:indexPath.row] objectForKey:@"replydate"]])
    {
    cell.lblMsgTitle.text=[[messageListArr objectAtIndex:indexPath.row] objectForKey:@"messagetype"];
    cell.lblMsgDescription.text=@"Awaiting response";
    cell.lblTime.text=[self dateForString:[[messageListArr objectAtIndex:indexPath.row ]objectForKey:@"messagedate"]];
        
    }
 else
 {
     if([[[[messageListArr objectAtIndex:indexPath.row]objectForKey:@"isReplyRead"]stringValue]isEqualToString:@"0"])
     {
         cell.lblMsgTitle.text=[[messageListArr objectAtIndex:indexPath.row] objectForKey:@"messagetype"];
         [cell.lblMsgTitle setFont:[UIFont fontWithName:FONT_SEGOE_BOLD size:29.0f]];
         [cell.lblMsgTitle setTextColor:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]];

        cell.lblTime.text=[self dateForString:[[messageListArr objectAtIndex:indexPath.row ]objectForKey:@"replydate"]];
         [cell.lblTime setTextColor:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]];
         
         cell.lblMsgDescription.text=[[messageListArr objectAtIndex:indexPath.row] objectForKey:@"reply"];
         [cell.lblMsgDescription setFont:[UIFont fontWithName:FONT_SEGOE_NORMAL size:17.0]];
         [cell.lblMsgDescription setTextColor:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]];


     }
     else
     {
         cell.lblMsgTitle.text=[[messageListArr objectAtIndex:indexPath.row] objectForKey:@"messagetype"];
         [cell.lblMsgTitle setFont:[UIFont fontWithName:FONT_SEGOE_NORMAL size:29.0f]];
         [cell.lblMsgTitle setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
         [cell.lblTime setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
         
         cell.lblMsgDescription.text=[[messageListArr objectAtIndex:indexPath.row] objectForKey:@"reply"];
         [cell.lblMsgDescription setFont:[UIFont fontWithName:FONT_SEGOE_NORMAL size:17.0]];
         [cell.lblMsgDescription setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
         cell.lblTime.text=[self dateForString:[[messageListArr objectAtIndex:indexPath.row ]objectForKey:@"replydate"]];
     }
     
 }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        MessageVC *mvc=[[MessageVC alloc]initWithNibName:@"MessageVC" bundle:nil];
    mvc.messageID=[NSString stringWithFormat:@"%@",[[messageListArr objectAtIndex:indexPath.row] objectForKey:@"messageid"]];
        [self.navigationController pushViewController:mvc animated:YES];

}
#pragma mark- WebServices API..

-(void)getMessageList
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];

    NSString *url = [NSString stringWithFormat:@"MessageList?Id=%@&timezone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID],tzName];

    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/MessageList/
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            messageListArr =[[NSMutableArray alloc]init];
            [messageListArr addObjectsFromArray:[responseDict objectForKey:@"replyData"]];
            if(messageListArr.count>0)
            {
            [tblView reloadData];
            }
            else
            {
                [lblMsgError setHidden:NO];
                if([[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY] intValue]<=30 )
                {
                    [lblMsgError setText:@"No Messages"];
                }
                else
                {
                    [lblMsgError setText:@"This app is read only. You will no longer be able to receive messages."];
                }

            }
            
            NSLog(@"%@",messageListArr);
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}


-(NSString *)dateForString:(NSString *)serverDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date = [dateFormatter dateFromString:serverDate];
    
  //  NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSDayCalendarUnit|NSMonthCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:date];
    
    
    
//    NSDateComponents *dateComponent = [[NSDateComponents alloc] init];
//    dateComponent.day = components.day;
//    dateComponent.year = components.year;
//    dateComponent.month = components.month;
    
   // NSDate *apiDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
   // [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
   // NSDate *defaultDate = [dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]];
    
    //if ([apiDate compare:defaultDate]==NSOrderedSame) {
        
//        NSDateComponents *comp1 = [[NSDateComponents alloc] init];
//        comp1.hour = components.hour;
//        comp1.minute = components.minute;
//        NSDate *onlyTime = [[NSCalendar currentCalendar] dateFromComponents:comp1];
//        [dateFormatter setDateFormat:@"MMM dd hh:mm a"];
//        [dateFormatter stringFromDate:onlyTime];
//        NSString *srvrDateString;
//        srvrDateString=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:onlyTime]];
//        return srvrDateString;
  //  }
  //  else
//    {
        [dateFormatter setDateFormat:@"MMM dd hh:mm a"];
        NSString *srvrDateString;
        [dateFormatter stringFromDate:date];
        srvrDateString=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
        return srvrDateString;
//    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
