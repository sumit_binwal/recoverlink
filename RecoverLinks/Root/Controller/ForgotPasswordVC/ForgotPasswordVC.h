//
//  ForgotPasswordVC.h
//  RecoverLinks
//
//  Created by  on 18/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController
- (IBAction)verifyButtonClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *verifyButton;
@property (strong, nonatomic) IBOutlet UIButton *resetButton;




@end
