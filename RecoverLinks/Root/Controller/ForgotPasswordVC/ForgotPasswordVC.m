//
//  ForgotPasswordVC.m
//  RecoverLinks
//
//  Created by  on 18/03/15.
//  Copyright (c) 2015 . All rights reserved.
//
#import "LoginVC.h"
#import "ForgotPasswordVC.h"

@interface ForgotPasswordVC ()<UIAlertViewDelegate,UITextFieldDelegate>
{
    
    IBOutlet UITextField *txtUsername;
    IBOutlet UITextField *txtMrnNo;
    IBOutlet NSLayoutConstraint *topConstraint;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtConfirmPassword;
    NSString *validToken;
    UITextField *activeTxtField;
    BOOL isPasswordSucessFullyResetFlag;
    IBOutlet UILabel *lblNOte;
}
@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    isPasswordSucessFullyResetFlag=false;
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"RESET PASSWORD"];
    [txtPassword setHidden:YES];
    [txtConfirmPassword setHidden:YES];
    [self.resetButton setHidden:YES];
    
    UIView *v4=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    txtUsername.leftView=v4;
    txtUsername.leftViewMode=UITextFieldViewModeAlways;
    
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    txtMrnNo.leftView=v1;
    txtMrnNo.leftViewMode=UITextFieldViewModeAlways;

    UIView *v2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    txtPassword.leftView=v2;
    txtPassword.leftViewMode=UITextFieldViewModeAlways;
    
    UIView *v3=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    txtConfirmPassword.leftView=v3;
    txtConfirmPassword.leftViewMode=UITextFieldViewModeAlways;
    
    //------ TextField Placeholder Color Change Code Start -------//
    [txtMrnNo setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtPassword setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
            forKeyPath:@"_placeholderLabel.textColor"];
    [txtConfirmPassword setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
            forKeyPath:@"_placeholderLabel.textColor"];
    [txtUsername setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
                      forKeyPath:@"_placeholderLabel.textColor"];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)verifyButtonClicked:(id)sender {
        [self.view endEditing:YES];

    if([CommonFunctions isValueNotEmpty:txtUsername.text])
    {

        if ([CommonFunctions isValueNotEmpty:txtMrnNo.text]) {
            if([CommonFunctions reachabiltyCheck])
            {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self validateMrnNumber];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
            }
        }
        else
        {
            activeTxtField=txtMrnNo;
            [CommonFunctions alertTitle:@"" withMessage:@"Please enter MRN first."withDelegate:self ];
        }
    }
    else
    {
        activeTxtField=txtUsername;
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter Username first."withDelegate:self ];
    }
}

-(BOOL)resetPassValidate
{
    BOOL issucess=false;
    if(![CommonFunctions isValueNotEmpty:txtPassword.text])
    {
        activeTxtField=txtPassword;
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter Password first." withDelegate:self];
        return issucess;
    }
    else if ([CommonFunctions trimSpaceInString:txtPassword.text].length<8)
    {
        
        [CommonFunctions alertTitle:@"" withMessage:@"Password must contain 8 or more characters."];
        return NO;
    }
    else if (![CommonFunctions IsValidPassword:txtPassword.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Password must have. \n 1. At least one upper case letter \n 2. At least one lower case letter. \n 3. At least one special character.  \n 4. At least one numeric value. \n 5. At least 8 character long."];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtConfirmPassword.text])
    {
        activeTxtField=txtConfirmPassword;
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter Confirm - Password first." withDelegate:self];
        return issucess;
    }
    else if (![txtPassword.text isEqualToString:txtConfirmPassword.text])
    {
        activeTxtField=txtPassword;
        [CommonFunctions alertTitle:@"" withMessage:@"Passwords do not match" withDelegate:self];
        return issucess;
    }
    return issucess=true;
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (isPasswordSucessFullyResetFlag)
    {
        LoginVC *lvc=[[LoginVC alloc]init];
        [self.navigationController pushViewController:lvc animated:YES];
    }
    else if(activeTxtField==txtUsername)
    {
        [txtUsername becomeFirstResponder];
    }
    else if(activeTxtField==txtMrnNo)
    {
        [txtMrnNo becomeFirstResponder];
    }
    else if (activeTxtField==txtPassword)
    {
        [txtPassword becomeFirstResponder];
         [txtPassword setText:@""];
         [txtConfirmPassword setText:@""];
    }
    else if (activeTxtField==txtConfirmPassword)
    {
        [txtConfirmPassword becomeFirstResponder];
        [txtConfirmPassword setText:@""];
    }
    

}
- (IBAction)passwordButtonCLicked:(id)sender {
    [self.view endEditing:YES];
    if([self resetPassValidate])
    {
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self resetPassword];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Check your network connection."];
        }
    }

}

#pragma mark- UITextField Delegate Method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTxtField=textField;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==txtMrnNo)
    {
        [textField resignFirstResponder];
    }
    else if (textField==txtPassword)
    {
        [txtConfirmPassword becomeFirstResponder];
    }
    else if (textField==txtConfirmPassword)
    {
        [textField resignFirstResponder];
    }
   
    return textField;
}

#pragma mark- WebService API

-(void)validateMrnNumber
{
        NSString *strMrnNo=[CommonFunctions trimSpaceInString:txtMrnNo.text];
    NSString *url = [NSString stringWithFormat:@"validateUserNameWithMRN"];
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtUsername.text],@"username",strMrnNo,@"mrn",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://recoverlink.reliablesandbox.net/api/Mobile/validateUserNameWithMRN
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            
            [txtMrnNo setHidden:YES];
            [self.verifyButton setHidden:YES];
            topConstraint.constant=-153.0f;
            [txtPassword setHidden:NO];
            [txtConfirmPassword setHidden:NO];
            [self.resetButton setHidden:NO];
            [txtUsername setHidden:YES];
            [lblNOte setHidden:YES];
            validToken=[responseDict objectForKey:@"replyData"];

            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}
-(void)resetPassword
{
     NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:validToken,@"token",txtConfirmPassword.text,@"password", nil];
    NSString *url = [NSString stringWithFormat:@"ResetPassword"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/ResetPassword

    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {


            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self];
            isPasswordSucessFullyResetFlag=true;
   

            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}

@end

