//
//  ChartVC.m
//  RecoverLinks
//
//  Created by  on 24/02/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "ChartVC.h"
#import "BarGraphVC.h"
#import "ProgramTrackerVC.h"
#import "FaceTrackerVC.h"
@interface ChartVC ()<UIScrollViewDelegate>
{
    IBOutlet NSLayoutConstraint *VConstratint;
    IBOutlet NSLayoutConstraint *VerticallySpace;
    IBOutlet UIScrollView *scrollVw;
    IBOutlet UIPageControl *pageController;
    
}
@end

@implementation ChartVC
@synthesize ptVC,ftVC,bgVC;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [self setupView];
    // Do any additional setup after loading the view from its nib.

    
}
-(void)viewDidLayoutSubviews
{

    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [scrollVw setScrollEnabled:YES];
    scrollVw.contentSize = CGSizeMake(scrollVw.contentSize.width,380.0f);
        scrollVw.zoomScale = 100.0f;
    }
    else
    {
     scrollVw.contentSize = CGSizeMake(scrollVw.contentSize.width,44.0f);
        scrollVw.zoomScale = 100.0f;    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [APPDELEGATE.cstmTabBar setHidden:NO];

    [ptVC viewWillAppear:YES];
    [ftVC viewWillAppear:YES];
    [bgVC viewWillAppear:YES];

}
-(void)setupView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"CHART"];
    
}


-(void)setupImgViewScrollView
{
    UIImageView *imagView1,*imageView2;
    CGRect rect=[[UIScreen mainScreen] bounds];
    float xpoint;
    for (int i = 0; i < 2; i++)
    {
        CGRect frame;
        frame.origin.x = rect.size.width * i;
        frame.origin.y = 0;
        frame.size.width = rect.size.width;
        frame.size.height = rect.size.height;;
        xpoint=rect.size.width;
        
        if(i==0)
        {
            ptVC = [[ProgramTrackerVC alloc] init];
            ptVC.view.frame = frame;
            [ptVC.view setBackgroundColor:[UIColor grayColor]];
            [scrollVw addSubview:ptVC.view];
        }else
        {

            ftVC.view.frame = frame;

        [ftVC.view setBackgroundColor:[UIColor blackColor]];
        [scrollVw addSubview:ftVC.view];
        }
        
    }
    scrollVw.contentSize=CGSizeMake(rect.size.width*2,self.view.frame.size.height);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageController.currentPage = page;
}
@end
