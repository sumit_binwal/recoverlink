//
//  ChartVC.h
//  RecoverLinks
//
//  Created by  on 24/02/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaceTrackerVC.h"
#import "ProgramTrackerVC.h"
#import "BarGraphVC.h"
@interface ChartVC : UIViewController
- (IBAction)ProgramTrackerButtonClicked:(id)sender;
- (IBAction)MoodTrackerButtonClicked:(id)sender;
- (IBAction)WeightTrackerButtonClicked:(id)sender;

@property (strong, nonatomic) IBOutlet FaceTrackerVC *ftVC;
@property (strong, nonatomic) IBOutlet ProgramTrackerVC *ptVC;
@property (strong, nonatomic) IBOutlet BarGraphVC *bgVC;
@end
