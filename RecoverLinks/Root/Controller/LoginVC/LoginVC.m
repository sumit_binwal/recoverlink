//
//  LoginVC.m
//  SampleProject
//
//  Created by  on 17/02/15.
//  Copyright (c) 2015   Infosolutions Pvt Ltd. All rights reserved.
//

#import "LoginVC.h"
#import "WelcomeVC.h"
#import "LearnVC.h"
#import "RegistrationVC.h"
#import "ChartVC.h"
#import "ContactVC.h"
#import "HelpVC.h"
#import "TipOfTheDayVC.h"
#import "ProgCompleteVC.h"
#import "ForgotPasswordVC.h"
@interface LoginVC ()<UITextFieldDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet NSLayoutConstraint *logoBottomCnstraint;
    IBOutlet NSLayoutConstraint *logoTopCnstratnt;
    UITextField *tfActiveTxt;
    IBOutlet UIImageView *imgVw;
    IBOutlet UITextField *txtUsername;
    IBOutlet UITextField *txtPassword;
    IBOutlet UIScrollView *scrllVw;
    NSDictionary *dataDict;
    IBOutlet UIButton *btnResetPass;

    
}

@end

@implementation LoginVC
#pragma mark- View Cycle Method
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupView];
    
    
    
    
    
}

-(void)setupView
{
    btnResetPass.contentMode=UIViewContentModeRedraw;
    imgVw.layer.cornerRadius = imgVw.frame.size.width/2.0;
    imgVw.clipsToBounds = YES;
    NSLog(@"Image URL : %@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_HOSPITAL_LOGO]);
        NSString *str=[[NSUserDefaults standardUserDefaults]objectForKey:UD_HOSPITAL_LOGO];
    if(str.length>0)
    {
//        [imgVw setImageWithURL:[NSURL URLWithString:str] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [imgVw sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageProgressiveDownload completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [CommonFunctions setNavigationBar:self.navigationController];
    
    //------Attributed Label In Navigation Bar..
    UILabel *titleLabel = [[UILabel alloc]init];
    
    NSDictionary *attributes1 = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                  NSFontAttributeName: [UIFont fontWithName:FONT_SEGOE_NORMAL size:20.0]
                                  };
    NSDictionary *attributes2 = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                  NSFontAttributeName: [UIFont fontWithName:FONT_SEGOE_BOLD size:20.0]
                                  };
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"WELCOME TO RecoverLINK"];
    [string addAttributes:attributes1 range:NSMakeRange(0, 18)];
    [string addAttributes:attributes2 range:NSMakeRange(18,4)];
    titleLabel.attributedText=string;
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    [txtPassword setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtUsername setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    
    //--Attributed Label In Navigation Bar End ..
    
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    txtUsername.leftView=v1;
    txtUsername.leftViewMode=UITextFieldViewModeAlways;
    
    UIView *v2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    txtPassword.leftView=v2;
    txtPassword.leftViewMode=UITextFieldViewModeAlways;
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
    // For 4s Screen
    if([[UIScreen mainScreen]bounds].size.height<568)
    {
        [scrllVw setScrollEnabled:NO];
        logoBottomCnstraint.constant=15.0f;
        logoTopCnstratnt.constant=20.0f;
    }
    else
    {
        [scrllVw setScrollEnabled:NO];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    txtPassword.text=@"";
    txtUsername.text=@"";
    if([[UIScreen mainScreen]bounds].size.height<568)
    {
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, 500.0f)];    }
    else
    {
        [scrllVw setScrollEnabled:NO];
    }
    
   

   
    
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if(tfActiveTxt)
    {
        [scrllVw setContentSize:CGSizeMake(scrllVw.bounds.size.width, 700.0f)];
    }
    else
    {
        [scrllVw setContentSize:CGSizeMake(scrllVw.bounds.size.width, 200.0f)];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Custome Selector Methods
-(void)tapGestureClicked
{
    [txtUsername resignFirstResponder];
    [txtPassword resignFirstResponder];
    [scrllVw setContentSize:CGSizeMake(scrllVw.bounds.size.width, self.view.bounds.size.height)];
    [scrllVw setContentOffset:CGPointZero];
}

#pragma mark- UITexField Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    tfActiveTxt=textField;
    [textField setBackground:[UIImage imageNamed:@"activeTextBox"]];
    [textField setTextColor:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]];
    [textField setValue:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]
             forKeyPath:@"_placeholderLabel.textColor"];
    
    [scrllVw setScrollEnabled:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 700.0f)];
    [self scrollViewToCenterOfScreen:textField];
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    tfActiveTxt=nil;
    if(textField==txtUsername)
    {
        
        [textField setBackground:[UIImage imageNamed:@"textBox"]];
        [textField setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
        [textField setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
                 forKeyPath:@"_placeholderLabel.textColor"];
        
    }
    else
    {
        [textField setBackground:[UIImage imageNamed:@"textBox"]];
        [textField setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
        [textField setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
                 forKeyPath:@"_placeholderLabel.textColor"];
    }
    [scrllVw setScrollEnabled:NO];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==txtUsername)
    {
        [txtPassword becomeFirstResponder];
    }
    if(textField==txtPassword)
    {
        [textField resignFirstResponder];
        [scrllVw setContentOffset:CGPointZero];
    }
    return YES;
}


#pragma mark- IBAction Methods

- (IBAction)otherButton:(id)sender {
if([CommonFunctions reachabiltyCheck])
{
    RegistrationVC *regvc=[[RegistrationVC alloc]init];
    [self.navigationController pushViewController:regvc animated:YES];
}
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
}
- (IBAction)forgotPasswordButtonClicked:(id)sender {
    [self.view endEditing:YES];
    ForgotPasswordVC *fpvc=[[ForgotPasswordVC alloc]initWithNibName:@"ForgotPasswordVC" bundle:nil];
    [self.navigationController pushViewController:fpvc animated:YES];
}

- (IBAction)loginButtonClicked:(id)sender {
    
    [self.view endEditing:YES];
    if([self isValidate])
    {
        if([CommonFunctions reachabiltyCheck])
        {
            [scrllVw setContentOffset:CGPointZero];
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self loginHospitalData];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Check Your Network Connection."];
        }
        
    }
}


#pragma mark- UIScrollView

-(void)scrollViewToCenterOfScreen:(UITextField *)txtField
{
    float difference;
    
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 60.0f;
    
    CGFloat viewCenterY = txtField.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 100.0f;
    CGFloat y = viewCenterY - avaliableHeight / 4.0f;
    
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 700.0f)];
}

#pragma mark- Custome Methods
-(BOOL)isValidate
{
    if(![CommonFunctions isValueNotEmpty:txtUsername.text])
    {
        tfActiveTxt=txtUsername;
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter Username." withDelegate:self];
        [scrllVw setContentOffset:CGPointZero];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPassword.text])
    {
        tfActiveTxt=txtPassword;
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter Password." withDelegate:self];
        [scrllVw setContentOffset:CGPointZero];
        return NO;
    }
    return YES;
}


#pragma mark- Login  WebService..
-(void)loginHospitalData
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];

    NSLog(@"%@",tzName);
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtUsername.text],@"username",tzName,@"timezone",[CommonFunctions trimSpaceInString:txtPassword.text],@"password",pushDeviceToken,@"DeviceId", nil];

    NSLog(@"params is %@",params);
    
    
    NSString *url = [NSString stringWithFormat:@"Login"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/Login
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            dataDict=[[NSDictionary alloc]init];
            dataDict=responseDict;
            [self performSelectorOnMainThread:@selector(loginToHospital:) withObject:dataDict waitUntilDone:YES];
            
        }
        else if ([[responseDict objectForKey:@"replyCode"]isEqualToString:@"locked"])
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]withDelegate:self withTag:100];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}

-(void)loginToHospital:(NSDictionary *)dict
{
    
    
    
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"eventid"] forKey:UD_EVENT_ID];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"patientid"] forKey:UD_PATIENT_ID];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"fname"] forKey:UD_FIRST_NAME];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:UD_HOSPITAL_LOGO] forKey:UD_HOSPITAL_LOGO];
    
    if([self isNotNull:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"]])
    {
        [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"] forKey:UD_FIND_MORE];
    }
    else
    {
        
    }
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"day"] forKey:UD_DAY];
        [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"Status"] forKey:UD_STATUS];

    [[NSUserDefaults standardUserDefaults]synchronize];
    
    if([[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue] isEqualToString:@"1"])
    {
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
    }
    else
    {
        [APPDELEGATE registerForRemoteNotification];
    }
    
    if([[[dict objectForKey:@"replyData"] objectForKey:@"day"]intValue]<=30)
    {
        if ([[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Disenrolled"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Deceased"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Completed 30-days"])
        {
    

            [APPDELEGATE allocTabBarAfter30Days];
            APPDELEGATE.tabBar.selectedIndex=0;
            [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
            [APPDELEGATE.window setRootViewController:APPDELEGATE.tabBar];
            [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
            [APPDELEGATE.cstmTabBar setHidden:NO];

            
        }
        else
        {
            NSString *str1=[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue];
            NSLog(@"%@",str1);
            
            if([[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue] isEqualToString:@"1"])
            {
                TipOfTheDayVC *tv = [[TipOfTheDayVC alloc]initWithNibName:@"TipOfTheDayVC" bundle:nil];
                
                if ([[[dict objectForKey:@"replyData"] objectForKey:@"typeOftip"] isEqualToString:@"Url"])
                {
                    tv.videoUrl=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"Url"]];
                    tv.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"] ];
                }
                else
                {
                    if([self isNotNull:[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]])
                    {
                        tv.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"] ];
                    }
                    else
                    {
                        tv.tipofDay=[NSString stringWithFormat:@""];
                    }
                }
                
                [APPDELEGATE allocTabBarTipControllar:tv];
                [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
                APPDELEGATE.window.rootViewController = APPDELEGATE.tabBar;
                [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                [APPDELEGATE.cstmTabBar setHidden:NO];
            }
            else
            {
                [APPDELEGATE allocTabBarControllar];
                [APPDELEGATE.window setRootViewController:APPDELEGATE.tabBar];
                [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                [APPDELEGATE.cstmTabBar setHidden:NO];
            }

        }
        
    }
    else
    {
        [APPDELEGATE allocTabBarAfter30Days];
        APPDELEGATE.tabBar.selectedIndex=0;
        [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
        [APPDELEGATE.window setRootViewController:APPDELEGATE.tabBar];
        [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
        [APPDELEGATE.cstmTabBar setHidden:NO];
    }
}

#pragma mark-UIAlertView Deleget Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100) {
        ForgotPasswordVC *fpvc=[[ForgotPasswordVC alloc]initWithNibName:@"ForgotPasswordVC" bundle:nil];
        [self.navigationController pushViewController:fpvc animated:YES];
    }
}
@end
