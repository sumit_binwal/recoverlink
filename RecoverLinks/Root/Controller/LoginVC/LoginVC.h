//
//  LoginVC.h
//  SampleProject
//
//  Created by  on 17/02/15.
//  Copyright (c) 2015   Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController

- (IBAction)forgotPasswordButtonClicked:(id)sender;

- (IBAction)loginButtonClicked:(id)sender;
- (IBAction)otherButton:(id)sender;
@end
