//
//  MessageVC.h
//  RecoverLinks
//
//  Created by  on 11/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageVC : UIViewController
{
    NSDictionary *dataDict;
    NSString *messageID;
}
@property (nonatomic,strong)    NSDictionary *dataDict;
@property (nonatomic,strong)    NSString *srvrDate;
@property(nonatomic,strong)     NSString *messageID;
@end
