//
//  LearnDetailVC.m
//  RecoverLinks
//
//  Created by  on 19/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "LearnDetailVC.h"

@interface LearnDetailVC ()<UIWebViewDelegate,UIAlertViewDelegate,UIScrollViewDelegate>
{
    
    IBOutlet UIWebView *wbView;
    BOOL webpageNotFound;
}
@end

@implementation LearnDetailVC
@synthesize LearnURL,LearnVideoURL;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    NSString *str = [LearnURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if([self isValidURL:str])
    {
        [NSString stringWithFormat:@""];
        NSURL *wbUrl=[NSURL URLWithString:str];
        NSLog(@"%@",LearnURL);
        NSLog(@"%@",str);
        NSURLRequest *request=[NSURLRequest requestWithURL:wbUrl];
        wbView.suppressesIncrementalRendering = YES;
        [wbView loadRequest:request];
        [CommonFunctions showActivityIndicatorWithText:@"Please Wait"];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Webpage not found." withDelegate:self];
        webpageNotFound=true;
    }
   
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if ([_previousView isEqualToString:@"tips"]) {
        [self.navigationItem setTitle:@"TIPS"];
    }
    else
    {
        [self.navigationItem setTitle:@"LEARN"];
    }
}
-(BOOL)isValidURL:(NSString*)str
{
    BOOL isValidURL = NO;
    NSURL *candidateURL = [NSURL URLWithString:str];
    if (candidateURL && candidateURL.scheme && candidateURL.host)
        isValidURL = YES;
    return isValidURL;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    [CommonFunctions removeActivityIndicator];

    for (UIView *object in wbView.scrollView.subviews) {
        if ([NSStringFromClass([object class]) isEqualToString:@"UIWebPDFView"]) {
            UIView *pdfView = object;
            for (UIView *pdfObjectSubview in pdfView.subviews) {
                if ([NSStringFromClass([pdfObjectSubview class]) isEqualToString:@"UIPDFPageView"]) {
                    UIView *uiPDFPageView = pdfObjectSubview;
                    uiPDFPageView.layer.shadowOpacity = 0.0f;
                }
            }
        }
    }
        wbView.scrollView.delegate = self;
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //     Ignore NSURLErrorDomain error -999.
    if (error.code == NSURLErrorCancelled) return;
    
    // Ignore "Fame Load Interrupted" errors. Seen after app store links.
    if (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"]) return;
    
         [CommonFunctions removeActivityIndicator];
    [CommonFunctions alertTitle:@"Recover Link " withMessage:@"Server Error"];



}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (webpageNotFound) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    for (UIView *object in wbView.scrollView.subviews) {
        if ([NSStringFromClass([object class]) isEqualToString:@"UIWebPDFView"]) {
            UIView *pdfView = object;
            for (UIView *pdfObjectSubview in pdfView.subviews) {
                if ([NSStringFromClass([pdfObjectSubview class]) isEqualToString:@"UIPDFPageView"]) {
                    UIView *uiPDFPageView = pdfObjectSubview;
                    uiPDFPageView.layer.shadowOpacity = 0.0f;
                }
            }
        }
    }
//    for (UIView* subView in [wbView subviews])
//    {
//        if ([subView isKindOfClass:[UIScrollView class]]) {
//            for (UIView* shadowView in [subView subviews])
//            {
//                if ([shadowView isKindOfClass:[UIImageView class]]) {
//                    [shadowView setHidden:YES];
//                }
//            }
//        }
//    }
   
}
/*
 
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
