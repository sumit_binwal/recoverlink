//
//  TipOfTheDayVC.h
//  RecoverLinks
//
//  Created by  on 26/02/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, YoutubeVideoState){
    YOUTUBE_VIDEO_STATE_UNSTARTED   = -1,
    YOUTUBE_VIDEO_STATE_ENDED       = 0,
    YOUTUBE_VIDEO_STATE_PLAYING     = 1,
    YOUTUBE_VIDEO_STATE_PAUSED      = 2,
    YOUTUBE_VIDEO_STATE_BUFFERING   = 3,
    YOUTUBE_VIDEO_STATE_VIDEO_CUED  = 5
};

@interface TipOfTheDayVC : UIViewController
{
    NSString *tipofDay;
}

@property (nonatomic, readonly) YoutubeVideoState state;

@property (strong, nonatomic) IBOutlet UILabel *lblTip;
- (IBAction)findOutMoreBtnClicked:(id)sender;
@property (strong, nonatomic)NSString *tipofDay;
@property(strong, nonatomic)NSString *videoUrl;
@end
