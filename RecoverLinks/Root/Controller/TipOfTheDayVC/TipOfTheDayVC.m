//
//  TipOfTheDayVC.m
//  RecoverLinks
//
//  Created by  on 26/02/15.
//  Copyright (c) 2015 . All rights reserved.
//
#import "YTPlayerView.h"
#import "TipOfTheDayVC.h"
#import "LearnVC.h"
#import "LearnDetailVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import "WelcomeVC.h"

@interface TipOfTheDayVC ()<YTPlayerViewDelegate>
{
    IBOutlet YTPlayerView *viewYTPlayer;
    IBOutlet UIWebView *wbView;
    IBOutlet UILabel *lblVideoTips;
    IBOutlet NSLayoutConstraint *wbviewBtm;
    MPMoviePlayerViewController *_player;
    IBOutlet UIWebView *playrWbVw;
    IBOutlet NSLayoutConstraint *wbViewTop;
    IBOutlet UIButton *btnFindOutMore;
    IBOutlet UITextView *txtVwVideoText;
    IBOutlet UILabel *lblTipOfDay;
    IBOutlet UIScrollView *scrlVw;
    
    BOOL allowLoad;
}
@end

@implementation TipOfTheDayVC
@synthesize tipofDay;
@synthesize videoUrl;
#pragma mark- Life Cycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"TIPS"];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
    playrWbVw.scrollView.scrollEnabled =NO;
    
    [viewYTPlayer setDelegate:self];
    allowLoad = true;
    
     [APPDELEGATE.cstmTabBar setHidden:NO];
   
    
    
if([UIScreen mainScreen].bounds.size.height<568)
{
        [scrlVw setContentSize:CGSizeMake(320.0f, 660.0f)];
}
    else
    {
        [scrlVw setScrollEnabled:NO];
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
    [CommonFunctions showActivityIndicatorWithText:@""];
    [self getCurrentDay];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_FIND_MORE]);
   APPDELEGATE.cstmTabBar.hidden = NO;
    [APPDELEGATE.btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [scrlVw setScrollEnabled:YES];
        [scrlVw setContentSize:CGSizeMake(320.0f, 660.0f)];
    }
    else
    {
        [scrlVw setScrollEnabled:NO];
    }

    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_FIND_MORE];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(void)viewDidLayoutSubviews
{
       APPDELEGATE.cstmTabBar.hidden = NO;
    if(videoUrl.length>0)
    {
        [scrlVw setScrollEnabled:YES];
        [scrlVw setContentSize:CGSizeMake(320.0f, 400.0f+lblVideoTips.frame.size.height)];
    }
    else
    {
        [scrlVw setScrollEnabled:YES];
        [scrlVw setContentSize:CGSizeMake(320.0f, 180.0f+_lblTip.frame.size.height)];
    }
}

#pragma mark-Custome Methods
-(void)youTubeStarted:(NSNotification *)notification{
    // your code here
    
    YoutubeVideoState currentState = self.state;
    
    if (currentState == YOUTUBE_VIDEO_STATE_PLAYING) {
        // Start your engines!
        
        NSLog(@"hiii");
    }
    
    //[APPDELEGATE.cstmTabBar setHidden:YES];
}

-(void)youTubeFinished:(NSNotification *)notification{
    // your code here
     [APPDELEGATE.cstmTabBar setHidden:NO];
}

- (NSString *)extractYoutubeID:(NSString *)youtubeURL
{
    //NSLog(@"youtube  %@",youtubeURL);
    NSError *error = NULL;
    NSString *regexString = @"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:NSRegularExpressionCaseInsensitive error:&error];
    NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:youtubeURL options:0 range:NSMakeRange(0, [youtubeURL length])];
    if(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0)))
    {
        NSString *substringForFirstMatch = [youtubeURL substringWithRange:rangeOfFirstMatch];
        return substringForFirstMatch;
    }
    return nil;
}

-(void)setUpView
{
    
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"TIPS"];
    [self.navigationItem setHidesBackButton:YES animated:YES];

    if(videoUrl.length>0)
    {
        [_lblTip setHidden:YES];
        
        NSString *youTubeID = [self extractYoutubeID:videoUrl];   // url is youtube url
        
        
        NSDictionary *playerVars = @{
                                     @"modestbranding":@1,@"showinfo": @0,
                                     @"controls":@0,@"rel":@0
                                     
                                     };
        //[webView loadWithVideoId:@"M7lc1UVf-VE" playerVars:playerVars];
        
        [viewYTPlayer loadWithVideoId:youTubeID playerVars:playerVars];
    
        
        
        
        if([tipofDay isEqualToString:@"<null>"])
        {
            lblVideoTips.text=@"";
        }
        else
        {
            lblVideoTips.text=tipofDay;
            [lblVideoTips sizeToFit];
        }
        
        [btnFindOutMore setHidden:YES];
    }
    else
    {
        [viewYTPlayer setHidden:YES];
        wbviewBtm.constant=0;
        wbViewTop.constant=0;
        [txtVwVideoText setHidden:YES];
        
        if([tipofDay isEqualToString:@"<null>"])
        {
            _lblTip.text=@"";
        }
        else
        {
            _lblTip.text=tipofDay;
            [_lblTip sizeToFit];
        }
        
    }
    
    [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
    
}


- (YoutubeVideoState)state
{
    return (YoutubeVideoState)[[playrWbVw stringByEvaluatingJavaScriptFromString:@"getPlayerState()"] integerValue];
}



//- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
//    if ([[[request URL] absoluteString] hasPrefix:@"somefakeurlscheme://video-ended"]) {
//        NSLog(@"Video Ended.. call");
//        [APPDELEGATE.cstmTabBar setHidden:YES];
//        return NO; // prevent really loading the URL
//    }
//    else
//    {
//    [APPDELEGATE.cstmTabBar setHidden:NO];
//    }
//    return allowLoad;
//}

- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    switch (state) {
        case kYTPlayerStatePlaying:
            
        { NSLog(@"Started playback");
            
            if ([CommonFunctions reachabiltyCheck]) {
                [self tracActivityAPI];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Check your network connection first"];
            }
            [APPDELEGATE.cstmTabBar setHidden:NO];
            
        }
            
            break;
        case kYTPlayerStatePaused:
            NSLog(@"Paused playback");
            break;
        default:
            break;
    }
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ( [[[request URL] scheme] isEqualToString:@"callback"] ) {
   [APPDELEGATE.cstmTabBar setHidden:NO];
        NSLog(@"Call Back Called");
        return YES;
    }
    else  if ( [[[request URL] scheme] isEqualToString:@"start"] ) {
        if ([CommonFunctions reachabiltyCheck]) {
        [self tracActivityAPI];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Check your network connection first"];
        }
   [APPDELEGATE.cstmTabBar setHidden:YES];

        return YES;
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView*)webView {
    
    NSLog(@"%@",[wbView stringByEvaluatingJavaScriptFromString:@"myFunction()"]);
    allowLoad = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideTabBAr
{
    [APPDELEGATE.cstmTabBar setHidden:YES];
}

-(void)showTabBAr
{
    [APPDELEGATE.cstmTabBar setHidden:NO];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)findOutMoreBtnClicked:(id)sender2 {

    if ([CommonFunctions reachabiltyCheck]) {
        [self tracFindOutMoreActivityAPI];
        LearnDetailVC *ldvc=[[LearnDetailVC alloc]initWithNibName:@"LearnDetailVC" bundle:nil];
        ldvc.previousView=@"tips";
        ldvc.LearnURL=[[NSUserDefaults standardUserDefaults]objectForKey:UD_FIND_MORE];
        [self.navigationController pushViewController:ldvc animated:YES];

    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your network connection first."];
    }
}


-(void)getCurrentDay
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSString *eventID=[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID];
    NSString *url = [NSString stringWithFormat:@"getDay?Id=%@&timezone=%@",eventID,tzName];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/getDay?Id=2033&timezone=-3:00
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
                        [self performSelectorOnMainThread:@selector(getActualDayView:) withObject:responseDict waitUntilDone:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}

-(void)getActualDayView:(NSMutableDictionary*)dict
{
       [APPDELEGATE.cstmTabBar setHidden:NO];
    NSLog(@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"]);
    
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"dayNo"] forKey:UD_DAY];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"Status"] forKey:UD_STATUS];
    if([self isNotNull:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"]])
    {
        [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"] forKey:UD_FIND_MORE];
    }
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"SetDailySurvey"] forKey:UD_SURVAY];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString *str=[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue];
    if([str isEqualToString:@"1"])
    {
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        [APPDELEGATE registerForRemoteNotificationNextDay];
    }
    else
    {
        [APPDELEGATE registerForRemoteNotification];
    }
    if([[[dict objectForKey:@"replyData"] objectForKey:@"dayNo"]intValue]<=30)
    {
        if ([[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Disenrolled"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Deceased"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Completed 30-days"])
        {
            [APPDELEGATE allocTabBarAfter30Days];
            APPDELEGATE.setUpTabBar;
            APPDELEGATE.tabBar.selectedIndex=0;
            [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
            [APPDELEGATE.cstmTabBar setHidden:NO];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
        }
        else
        {
            if([[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue] isEqualToString:@"1"])
            {
                if ([[[dict objectForKey:@"replyData"] objectForKey:@"typeOftip"] isEqualToString:@"Url"]) {
                    videoUrl=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"Url"]];
                    allowLoad = true;
                    [self setUpView];
                    [viewYTPlayer  setHidden:NO];
                    lblVideoTips.text=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]];
                    [scrlVw setScrollEnabled:YES];
                    [scrlVw setContentSize:CGSizeMake(320.0f, 180.0f+lblVideoTips.frame.size.height)];
                    [_lblTip setHidden:YES];
                    
                }
                else
                {
                    if([self isNotNull:[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]])
                    {
                        tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"] ];

                        [scrlVw setScrollEnabled:YES];
                        [scrlVw setContentSize:CGSizeMake(320.0f, 180.0f+_lblTip.frame.size.height)];
                        [viewYTPlayer setHidden:YES];
                        wbviewBtm.constant=0;
                        wbViewTop.constant=0;
                        [txtVwVideoText setHidden:YES];
                        
                        if([tipofDay isEqualToString:@"<null>"])
                        {
                            _lblTip.text=@"";
                        }
                        else
                        {
                            _lblTip.text=tipofDay;
                            [_lblTip sizeToFit];
                            
                            if([self isNotNull:[[NSUserDefaults standardUserDefaults]objectForKey:UD_FIND_MORE]])
                            {
                                [btnFindOutMore setHidden:NO];
                            }
                            else
                            {
                                [btnFindOutMore setHidden:YES];
                            }
                            
                        }
                    }
                }
            }
            else
            {
                WelcomeVC *wvc=[[WelcomeVC alloc]initWithNibName:@"WelcomeVC" bundle:nil];
                [APPDELEGATE allocTabBarTipControllar:wvc];
                APPDELEGATE.setUpTabBar;
                APPDELEGATE.tabBar.selectedIndex=0;
                [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                [APPDELEGATE.cstmTabBar setHidden:NO];
                [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
                [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
            }
        }
    }
    else
    {
        [APPDELEGATE allocTabBarAfter30Days];
        APPDELEGATE.setUpTabBar;
        APPDELEGATE.tabBar.selectedIndex=0;
        [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
        [APPDELEGATE.cstmTabBar setHidden:NO];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
    }
}


-(void)tracActivityAPI
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSString *eventID=[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID];

    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"3",@"TractId",eventID,@"EventId",tzName,@"timezone", nil];
    
    
    NSString *url = [NSString stringWithFormat:@"DevicePlayTipVideoClick"];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];

    //http://192.168.0.152:99/api/Mobile/DevicePlayTipVideoClick
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            NSLog(@"%@",responseDict);
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}
-(void)tracFindOutMoreActivityAPI
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSString *eventID=[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID];
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"4",@"TractId",eventID,@"EventId",tzName,@"timezone", nil];
    
    
    NSString *url = [NSString stringWithFormat:@"DeviceFindOutMoreClick"];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    //http://192.168.0.152:99/api/Mobile/DeviceFindOutMoreClick
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            NSLog(@"%@",responseDict);
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}


@end
