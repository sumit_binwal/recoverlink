//
//  QuestionVC.m
//  RecoverLinks
//
//  Created by  on 11/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "QuestionVC.h"
#import "MessageListVC.h"
@interface QuestionVC ()<UITextFieldDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate>
{
    
    IBOutlet UIImageView *txtVwImage;
    IBOutlet UILabel *lblTextView;
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UITextView *txtMessage;
    IBOutlet UITextField *txtReason;
    IBOutlet UIView *viewPicker;
    UITextField *activeTextFld;
    UITextView *activeTextView;
    NSMutableArray *reasonArray;
    NSString *reasonID;
    BOOL sucessFullyMsgPost;
    BOOL flagForTextFldIdentification;
    BOOL flagForTextViewIdentification;
}

@end

@implementation QuestionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
// Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"QUESTIONS"];
    UIView *view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
    
    //set TxtReason Text Field Padding...
    txtReason.leftView=view1;
    txtReason.leftViewMode=UITextFieldViewModeAlways;

    txtReason.inputView=viewPicker;
    [txtMessage setReturnKeyType: UIReturnKeyDone];

    /**
     *  Chage PlaceHolder Color
     */
    [txtReason setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    

    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
            [self fetchReason];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check Your Network Connection First."];
    }
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, 470.0f)];
    }
    else
    {
        [scrllVw setScrollEnabled:NO];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Custome Action Methods
-(IBAction)cancleButtonClicked:(id)sender
{
    [txtReason resignFirstResponder];
    [scrllVw setContentOffset:CGPointZero];
    txtReason.text=@"";
}
-(IBAction)doneButtonClicked:(id)sender
{
   if(txtReason.text.length<1)
   {
       txtReason.text=[[reasonArray objectAtIndex:0] objectForKey:@"MessageType"];
       reasonID=[NSString stringWithFormat:@"%@",[[reasonArray objectAtIndex:0] objectForKey:@"Id"]];
   }
    [txtMessage becomeFirstResponder];
}

#pragma mark- ScrollVw Methods

-(void)scrollViewToCenterOfScreen:(UITextField *)txtField
{
    float difference;
    
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 60.0f;
    
    CGFloat viewCenterY = txtField.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 100.0f;
    CGFloat y = viewCenterY - avaliableHeight / 4.0f;
    
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 700.0f)];
}
-(void)scrollViewToCenterOfScreenTxtViw:(UITextView *)txtVw
{
    float difference;
    
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 60.0f;
    
    CGFloat viewCenterY = txtVw.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 100.0f;
    CGFloat y = viewCenterY - avaliableHeight / 4.0f;
    
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 2000.0f)];
}

#pragma mark- UITextField Delegate Methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==txtReason)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{

    [textField setBackground:[UIImage imageNamed:@"activeTextBox"]];
    [textField setTextColor:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]];
    [textField setValue:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]
             forKeyPath:@"_placeholderLabel.textColor"];
    
    [scrllVw setScrollEnabled:YES];
    
    if(textField==txtReason)
    {
           }
    
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, 600.0f)];
    }
    else
    {
               [scrllVw setContentSize:CGSizeMake(320.0f, 650.0f)];
    }
  [self scrollViewToCenterOfScreen:textField];

}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField setBackground:[UIImage imageNamed:@"textBox"]];
    [textField setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
    [textField setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
             forKeyPath:@"_placeholderLabel.textColor"];
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, 480.0f)];
    }
    else
    {
        [scrllVw setScrollEnabled:NO];
        [scrllVw setContentSize:CGSizeMake(320.0f, 650.0f)];
    }
}

#pragma mark-UiTextView Delegate Methods
-(void)textViewDidChange:(UITextView *)textView
{
    if(![textView hasText]) {
        lblTextView.hidden = NO;
    }
    else{
        lblTextView.hidden = YES;
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [txtMessage resignFirstResponder];
        
        if([UIScreen mainScreen].bounds.size.height<568)
        {
            [scrllVw setScrollEnabled:YES];
                        [scrllVw setContentOffset:CGPointZero];
            [scrllVw setContentSize:CGSizeMake(320.0f, 470.0f)];
        }
        else
        {
            [scrllVw setContentOffset:CGPointZero];
            [scrllVw setScrollEnabled:NO];

        }
    }


    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self scrollViewToCenterOfScreenTxtViw:textView];
    [txtVwImage setImage:[UIImage imageNamed:@"txtViewActive"]];
    [lblTextView setTextColor:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]];
        [scrllVw setScrollEnabled:YES];
    if([UIScreen mainScreen].bounds.size.height<568)
    {


        [scrllVw setContentSize:CGSizeMake(320.0f, 650.0f)];
    }
    else
    {

        [scrllVw setContentSize:CGSizeMake(320.0f, 650)];
        
    }

    
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    [txtVwImage setImage:[UIImage imageNamed:@"txtView"]];
    [lblTextView setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
    [txtMessage setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
    
}

#pragma mark- UIPickerView Delegate mthods..

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return reasonArray.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[reasonArray objectAtIndex:row]objectForKey:@"MessageType"];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    txtReason.text=[[reasonArray objectAtIndex:row]objectForKey:@"MessageType"];
    reasonID=[NSString stringWithFormat:@"%@",[[reasonArray objectAtIndex:row] objectForKey:@"Id"]];
    UITextPosition *beginning = [txtReason beginningOfDocument];
    [txtReason setSelectedTextRange:[txtReason textRangeFromPosition:beginning
                                                          toPosition:beginning]];

}

#pragma mark - WebService API

-(void)fetchReason
{
    
//    NSMutableDictionary *params;
    NSString *url = [NSString stringWithFormat:@"MessageTypes"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/MessageTypes
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            reasonArray =[[NSMutableArray alloc]init];
            [reasonArray addObjectsFromArray:[responseDict objectForKey:@"replyData"]];
            //  profileDataDict=[[responseDict objectForKey:@"userData"] objectForKey:@"User"];
            //[self performSelectorOnMainThread:@selector(updateEmail:) withObject:profileDataDict waitUntilDone:YES];
            NSLog(@"%@",reasonArray);
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}
-(void)sendMessageAPI
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    

    NSMutableDictionary *params;
    NSString *url = [NSString stringWithFormat:@"sendMessage"];
    params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID],@"eventId",txtMessage.text,@"Message",reasonID,@"MessageTypeId",tzName,@"timezone", nil];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/sendMessage
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [CommonFunctions alertTitle:@"" withMessage:@"If this is an emergency call 911. Your care team will respond during normal business hours 9am - 5pm Monday to Friday." withDelegate:self withTag:200];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}

#pragma mark - TextField Delegate methods
-(BOOL)validateTextField
{
    BOOL issucess=false;
    NSCharacterSet *whiteSpace=[NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *str=[txtReason.text stringByTrimmingCharactersInSet:whiteSpace];
    NSString *str1=[txtMessage.text stringByTrimmingCharactersInSet:whiteSpace];
    if(str.length == 0)
    {
    flagForTextFldIdentification=true;
        [CommonFunctions alertTitle:@"" withMessage:@"Please select reason first." withDelegate:self];
        return issucess;
    }
    else if (str1.length==0)
    {
    flagForTextViewIdentification=true;
        [txtMessage becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter message first." withDelegate:self];
        return issucess;
    }
    return issucess=true;
}

- (IBAction)submitButtonClicked:(id)sender {
    [self.view endEditing:YES];
    [scrllVw setContentOffset:CGPointZero];
    if([self validateTextField])
    {
       if([CommonFunctions reachabiltyCheck])
       {
           [CommonFunctions showActivityIndicatorWithText:@""];
           [self sendMessageAPI];
       }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Check Your Network Connection."];
        }
    }
}

#pragma mark-UIAlert View Delegate Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(flagForTextViewIdentification)
    {
        [txtMessage becomeFirstResponder];
        flagForTextViewIdentification=false;
    }
else if (flagForTextFldIdentification)
    {
        [txtReason becomeFirstResponder];
        flagForTextFldIdentification=false;
    }
    else if (alertView.tag==200)
    {
    [self.navigationController popViewControllerAnimated:NO];
    }
}
@end
