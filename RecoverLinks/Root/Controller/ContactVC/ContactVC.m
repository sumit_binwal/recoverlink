//
//  ContactVC.m
//  RecoverLinks
//
//  Created by  on 24/02/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "ContactVC.h"
#import "MessageListVC.h"
#import "QuestionVC.h"
#import "ContactListVC.h"
@interface ContactVC ()
{
    
    IBOutlet UIScrollView *scrlVw;

    IBOutlet UIImageView *lblImgVw;
    IBOutlet NSLayoutConstraint *msgBtmCnstraint;
    IBOutlet NSLayoutConstraint *msgTopCnstratint;
    IBOutlet NSLayoutConstraint *qstnTopCnstraint;
    NSMutableArray *messageListArr;
}
@end

@implementation ContactVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [APPDELEGATE.cstmTabBar setHidden:NO];
    
    [self setupView];
    
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        msgBtmCnstraint.constant=9.0f;
        msgTopCnstratint.constant=9.0f;
        qstnTopCnstraint.constant=9.0f;
    }
    else
    {
        [scrlVw setScrollEnabled:NO];
    }
    
    // Do any additional setup after loading the view from its nib.
}

-(void)setupView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"CONTACT"];
    [scrlVw setScrollEnabled:YES];
    

   }
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [scrlVw setContentOffset:CGPointZero];
    
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getMessageList];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check Your Network Connection First."];
    }
    
    NSString *str=[[NSTimeZone defaultTimeZone] localizedName:NSTimeZoneNameStyleDaylightSaving
                                                       locale:[NSLocale currentLocale]];
        NSLog(@"::::: %@",str);
    str=[[NSTimeZone defaultTimeZone] localizedName:NSTimeZoneNameStyleGeneric
                                                       locale:[NSLocale currentLocale]];
        NSLog(@"::::: %@",str);
    str=[[NSTimeZone defaultTimeZone] localizedName:NSTimeZoneNameStyleShortDaylightSaving
                                             locale:[NSLocale currentLocale]];
        NSLog(@"::::: %@",str);
    str=[[NSTimeZone defaultTimeZone] localizedName:NSTimeZoneNameStyleShortGeneric
                                             locale:[NSLocale currentLocale]];
        NSLog(@"::::: %@",str);
    str=[[NSTimeZone defaultTimeZone] localizedName:NSTimeZoneNameStyleShortStandard
                                             locale:[NSLocale currentLocale]];
        NSLog(@"::::: %@",str);
    str=[[NSTimeZone defaultTimeZone] localizedName:NSTimeZoneNameStyleStandard
                                             locale:[NSLocale currentLocale]];
        NSLog(@"::::: %@",str);
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    NSLog(@":Tzname : :::: %@",tzName);
    
    NSLog(@"::::: %@",str);
    str=[str stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    NSLog(@"::::: %@",str);
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [APPDELEGATE.cstmTabBar setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)questionButtonClicked:(id)sender {

    if([[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY] intValue]<=30 )
    {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:UD_STATUS]isEqualToString:@"Disenrolled"]||[[[NSUserDefaults standardUserDefaults]objectForKey:UD_STATUS]isEqualToString:@"Deceased"]||[[[NSUserDefaults standardUserDefaults]objectForKey:UD_STATUS]isEqualToString:@"Completed 30-days"])
        {
            MessageListVC *mlvc=[[MessageListVC alloc] init];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"fromQuestion" forKey:@"vcName"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self.navigationController pushViewController:mlvc animated:YES];

        }
        else
        {
            QuestionVC *qvc=[[QuestionVC alloc]init];
            [self.navigationController pushViewController:qvc animated:YES];
            
        }
    }
    else
    {
        MessageListVC *mlvc=[[MessageListVC alloc] init];
        [[NSUserDefaults standardUserDefaults]setValue:@"fromQuestion" forKey:@"vcName"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self.navigationController pushViewController:mlvc animated:YES];

    }
}
- (IBAction)messageButtonClicked:(id)sender {
    MessageListVC *mvc=[[MessageListVC alloc]initWithNibName:@"MessageListVC" bundle:nil];
    [self.navigationController pushViewController:mvc animated:YES];
}
- (IBAction)contactButtonClicked:(id)sender {
    ContactListVC *clvc=[[ContactListVC alloc]initWithNibName:@"ContactListVC" bundle:nil];
    [self.navigationController pushViewController:clvc animated:YES];
}

#pragma mark- WebServiceAPI
-(void)getMessageList
{

    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSString *url = [NSString stringWithFormat:@"MessageList?Id=%@&timezone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID],tzName];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/MessageList/
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
        
            for (int i=0; i<[[responseDict objectForKey:@"replyData"]count]; i++) {
                if ([self isNotNull:[[[responseDict objectForKey:@"replyData"]objectAtIndex:i]objectForKey:@"isReplyRead"]]) {
                    
                    if ([[[[[responseDict objectForKey:@"replyData"]objectAtIndex:i]objectForKey:@"isReplyRead"] stringValue]isEqualToString:@"0"]) {
                        [lblImgVw setHidden:NO];
                        return;
                    }
                    else
                    {
                        [lblImgVw setHidden:YES];
                    }
                }
                else
                {
                    
                }
            }
            return;
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}




@end
