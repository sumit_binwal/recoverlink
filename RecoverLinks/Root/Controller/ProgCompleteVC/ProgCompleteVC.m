//
//  ProgCompleteVC.m
//  RecoverLinks
//
//  Created by  on 13/04/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "ProgCompleteVC.h"
#import "TipOfTheDayVC.h"
#import "WelcomeVC.h"
@interface ProgCompleteVC ()

@end

@implementation ProgCompleteVC
@synthesize lblDetail,lblHeading;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    [CommonFunctions showActivityIndicatorWithText:@""];
    [self getCurrentDay];
    
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:UD_STATUS]isEqualToString:@"Deceased"]||[[[NSUserDefaults standardUserDefaults]objectForKey:UD_STATUS]isEqualToString:@"Disenrolled"]||[[[NSUserDefaults standardUserDefaults]objectForKey:UD_STATUS]isEqualToString:@"Completed 30-days"]) {
        [self.navigationItem setTitle:@"PROGRAM ENDED"];
        lblDetail.text=@"This app is read-only now. Contact your case manager if you have any questions.";
        lblHeading.text=@"";
    }
    else
    {
        [self.navigationItem setTitle:@"PROGRAM COMPLETED"];
        lblDetail.text=@"You have completed the 30-day program, contact your case manager if you have any questions. This app is read-only now.";
        lblHeading.text=@"Congratulations!";
    }
    
      [APPDELEGATE.btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];
}
-(void)setupView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)getCurrentDay
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSString *eventID=[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID];
    NSString *url = [NSString stringWithFormat:@"getDay?Id=%@&timezone=%@",eventID,tzName];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/getDay?Id=2033&timezone=-3:00
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
                                    [self performSelectorOnMainThread:@selector(getActualDayView:) withObject:responseDict waitUntilDone:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}

-(void)getActualDayView:(NSMutableDictionary*)dict
{
    
    
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"dayNo"] forKey:UD_DAY];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"Status"] forKey:UD_STATUS];
    if([self isNotNull:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"]])
    {
        [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"] forKey:UD_FIND_MORE];
    }
    
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"SetDailySurvey"] forKey:UD_SURVAY];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString *str=[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue];
    
    if([str isEqualToString:@"1"])
    {
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        [APPDELEGATE registerForRemoteNotificationNextDay];
    }
    else
    {
        [APPDELEGATE registerForRemoteNotification];
    }
    if([[[dict objectForKey:@"replyData"] objectForKey:@"dayNo"]intValue]<=30)
    {
        if ([[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Disenrolled"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Deceased"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Completed 30-days"])
        {
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            [[UIApplication sharedApplication]cancelLocalNotification:APPDELEGATE.notification];
        }
        else
        {
            if([str isEqualToString:@"1"])
            {
                
                            TipOfTheDayVC *tvc=[[TipOfTheDayVC alloc]initWithNibName:@"TipOfTheDayVC" bundle:nil];
                                [APPDELEGATE allocTabBarTipControllar:tvc];
                                APPDELEGATE.setUpTabBar;
                                APPDELEGATE.tabBar.selectedIndex=0;
                                [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                                [APPDELEGATE.cstmTabBar setHidden:NO];
                                [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                                [[UIApplication sharedApplication] cancelAllLocalNotifications];
                                [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
                
                if ([[[dict objectForKey:@"replyData"] objectForKey:@"typeOftip"] isEqualToString:@"Url"]) {
                    tvc.videoUrl=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"Url"]];
                    NSLog(@"%@",tvc.videoUrl);
                    tvc.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]];
                }
                else
                {
                    if([self isNotNull:[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]])
                                            {
                                               tvc.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"] ];
                                            }
                                        }
                
                
            }
            else
            {
                WelcomeVC *wvc=[[WelcomeVC alloc]initWithNibName:@"WelcomeVC" bundle:nil];
                [APPDELEGATE allocTabBarTipControllar:wvc];
                
                APPDELEGATE.setUpTabBar;
                APPDELEGATE.tabBar.selectedIndex=0;
                [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                [APPDELEGATE.cstmTabBar setHidden:NO];
                [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

                [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
            }
        }
    }
    else
    {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [[UIApplication sharedApplication]cancelLocalNotification:APPDELEGATE.notification];

    }

    
}
@end
