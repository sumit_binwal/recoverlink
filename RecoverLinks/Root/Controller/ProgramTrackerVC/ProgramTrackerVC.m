//
//  ProgramTrackerVC.m
//  RecoverLinks
//
//  Created by  on 31/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "ProgramTrackerVC.h"
#import "AppTrackerCell.h"
@interface ProgramTrackerVC ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>
{
    
    IBOutlet UILabel *lblMsg;
    IBOutlet UIScrollView *scrllVw;

    NSArray *question3DataArr;
    NSArray *question4DataArr;
    NSMutableArray *AppDataArr;
}
@end

@implementation ProgramTrackerVC
@synthesize BreathCollectionView,ArmsCollectionView,AppCollectionView;

- (void)viewDidLoad {
    [super viewDidLoad];
        AppDataArr =[[NSMutableArray alloc]init];
    [AppCollectionView registerNib:[UINib nibWithNibName:@"AppTrackerCell" bundle:nil] forCellWithReuseIdentifier:@"AppTrackerCell"];
    [BreathCollectionView registerNib:[UINib nibWithNibName:@"AppTrackerCell" bundle:nil] forCellWithReuseIdentifier:@"AppTrackerCell"];
    [ArmsCollectionView registerNib:[UINib nibWithNibName:@"AppTrackerCell" bundle:nil] forCellWithReuseIdentifier:@"AppTrackerCell"];

    

       // Do any additional setup after loading the view from its nib.
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"CHART"];
    
    int dayis =[[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY]intValue];
    if(dayis>30)
    {
        NSString *str=[NSString stringWithFormat:@"You have Completed the 30-day program \n This app is read only now."];
        lblMsg.text=str;
    }
    else
    {
        int dayis =[[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY]intValue];
        int leftdayis=30-dayis;
        NSString *str=[NSString stringWithFormat:@"You are on DAY %d of the program. \n %d more days to go! ",dayis,leftdayis];
        
        NSMutableAttributedString *aString = [[NSMutableAttributedString alloc] initWithString:str];
        [aString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_SEGOE_BOLD size:14.0f]range:NSMakeRange(11, 6)];
        lblMsg.attributedText=aString;
        
    }

    

    
}

-(void)callPropgrameTrackerAPI
{
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getAppGraphData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to server."];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setUpView];
    [self callPropgrameTrackerAPI];


}
-(void)viewWillDisappear:(BOOL)animated
{

}


#pragma mark- UICollectionView Delegate Methods
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        return CGSizeMake(10.4f , 50.0f);
    }
    else
    {
    return CGSizeMake(7.5f , 40.0f);
    }
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
        return AppDataArr.count;

}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView==AppCollectionView)
    {
        AppTrackerCell *appTrackerCell=[AppCollectionView dequeueReusableCellWithReuseIdentifier:@"AppTrackerCell" forIndexPath:indexPath];
        
        if([[[AppDataArr objectAtIndex:indexPath.row] objectForKey:@"absent"]isEqualToString:@"true"])
        {
            [appTrackerCell.imgGraphBar setHidden:YES];
            [appTrackerCell.imgCross setHidden:NO];
        }
        else
        {
            [appTrackerCell.imgCross setHidden:YES];
            [appTrackerCell.imgGraphBar setHidden:NO];
            [appTrackerCell.imgGraphBar setBackgroundColor:[UIColor colorWithRed:28.0f/255.0f green:37.0f/255.0f blue:63.0f/255.0f alpha:1]];
        }
        return appTrackerCell;
    }
    
    else  if (collectionView==BreathCollectionView)
    {
        AppTrackerCell *appTrackerCell=[BreathCollectionView dequeueReusableCellWithReuseIdentifier:@"AppTrackerCell" forIndexPath:indexPath];
        NSLog(@"AppData arr %@",[AppDataArr objectAtIndex:indexPath.row]);
        
        if ([[[AppDataArr objectAtIndex:indexPath.row] valueForKey:@"Breath"] isEqualToString:@"True"])
        {
            [appTrackerCell.imgCross setHidden:YES];
            [appTrackerCell.imgGraphBar setHidden:NO];
            [appTrackerCell.imgGraphBar setBackgroundColor:[UIColor colorWithRed:185.0f/255.0f green:-0.0f/255.0f blue:36.0f/255.0f alpha:1]];
        }
        else if([[[AppDataArr objectAtIndex:indexPath.row] objectForKey:@"Breath"]isEqualToString:@"absent"])
        {
            [appTrackerCell.imgGraphBar setHidden:YES];
            [appTrackerCell.imgCross setHidden:NO];

        }
        
        else
        {
            [appTrackerCell.imgCross setHidden:YES];
            [appTrackerCell.imgGraphBar setHidden:NO];
            [appTrackerCell.imgGraphBar setBackgroundColor:[UIColor colorWithRed:136.0f/255.0f green:185.0f/255.0f blue:54.0f/255.0f alpha:1]];

        }
                    return appTrackerCell;
        
        
    }
    
    else if (collectionView==ArmsCollectionView)
    {
        AppTrackerCell *appTrackerCell=[ArmsCollectionView dequeueReusableCellWithReuseIdentifier:@"AppTrackerCell" forIndexPath:indexPath];
        
        if([[[AppDataArr objectAtIndex:indexPath.row] objectForKey:@"Swelling"]isEqualToString:@"absent"])
        {
            [appTrackerCell.imgGraphBar setHidden:YES];
            [appTrackerCell.imgCross setHidden:NO];
        }
        else if ([[[AppDataArr objectAtIndex:indexPath.row] objectForKey:@"Swelling"]isEqualToString:@"True"])
        {
                        [appTrackerCell.imgCross setHidden:YES];
            [appTrackerCell.imgGraphBar setHidden:NO];
            [appTrackerCell.imgGraphBar setBackgroundColor:[UIColor colorWithRed:185.0f/255.0f green:-0.0f/255.0f blue:36.0f/255.0f alpha:1]];
        }
        else
        {
            [appTrackerCell.imgCross setHidden:YES];
            [appTrackerCell.imgGraphBar setHidden:NO];
            [appTrackerCell.imgGraphBar setBackgroundColor:[UIColor colorWithRed:136.0f/255.0f green:185.0f/255.0f blue:54.0f/255.0f alpha:1]];
        
        }
        return appTrackerCell;
    }
    return nil;
}
#pragma mark- Calling WebService API
-(void)getAppGraphData
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];

    
    NSString *url = [NSString stringWithFormat:@"BreathAndSwellingGraph?Id=%@&timezone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID],tzName];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/BreathAndSwellingGraph/1
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);

        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            
            
            [self performSelectorOnMainThread:@selector(fetchAppData:) withObject:responseDict waitUntilDone:YES];
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}
-(void)fetchAppData:(NSDictionary *)dict
{

    AppDataArr=[dict objectForKey:@"replyData"];
    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"CurrentDay"] forKey:UD_DAY];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self setUpView];
    [AppCollectionView reloadData];
    [BreathCollectionView reloadData];
    [ArmsCollectionView reloadData];

 [CommonFunctions removeActivityIndicator];
}
@end
