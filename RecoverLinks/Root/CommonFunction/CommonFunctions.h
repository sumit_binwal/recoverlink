//
//  CommonFunctions.h
//  Metal Calculator
//
//  Created by  on 26/11/14.
//  Copyright (c) 2014   Infosolutions Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "ConnectionManager.h"
#import "UIImageView+WebCache.h"

@interface CommonFunctions : NSObject

+(void) setNavigationBar:(UINavigationController*)navController setNavItem:(UINavigationItem *)navItem ;
+(void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg;
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate;
+(void)showNetworkAlert;
+(NSString *)trimSpaceInString:(NSString *)mainstr;
+ (void)showActivityIndicatorWithText:(NSString *)text;
+ (void)removeActivityIndicator;
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg BtnTitle:(NSString*)btnName withDelegate:(id)delegate;
+(BOOL) reachabiltyCheck;
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate withTag:(int)tag;
+ (BOOL)isValueNotEmpty:(NSString*)aString;
+(BOOL)IsValidPassword:(NSString *)checkString;
+(BOOL)IsValidEmail:(NSString *)checkString;
+(void) setNavigationBar:(UINavigationController*)navController;
+(NSDate *)convertDateFromGMTFormat:(NSString *)date;
+(void) setSecondNavigationBar:(UINavigationController*)navController;
@end
