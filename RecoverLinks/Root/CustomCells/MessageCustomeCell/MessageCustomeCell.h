//
//  MessageCustomeCell.h
//  RecoverLinks
//
//  Created by  on 13/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblMsgDescription;

@property (strong, nonatomic) IBOutlet UIImageView *imgDevider;
@property (strong, nonatomic) IBOutlet UILabel *lblMsgTitle;
@end
