//
//  LearnCustomeCell.h
//  RecoverLinks
//
//  Created by  on 13/03/15.
//  Copyright (c) 2015 . All rights reserved.
//
#import "YTPlayerView.h"
#import <UIKit/UIKit.h>

@interface LearnCustomeCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgVwLearn;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblArticleLabel;
@property (strong, nonatomic) IBOutlet UIWebView *wbView;
@property (strong, nonatomic) IBOutlet YTPlayerView *viewYTPlayer;

@end
